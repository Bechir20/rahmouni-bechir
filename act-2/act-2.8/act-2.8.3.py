class Livre:
    def __init__(self, titre, auteur, prix, nbrpages, genre='action'):
        self.titre = titre
        self.auteur = auteur
        self.prix = prix
        self.nbrpages = nbrpages
        self.genre = genre


livre11 = Livre("Le petit prince", "StExupéry", 10.40, 50)
livre1 = Livre("Contes", "Grimm", 14.40, 254)
print(livre11.titre, livre11.auteur, livre11.prix, livre11.nbrpages, livre11.genre)
print(livre1.titre, livre1.auteur, livre1.prix, livre1.nbrpages, livre1.genre)


class BD(Livre):
    def __init__(self, titre, auteur, prix, nbrpages, is_color, genre='action', lecture='de gauche a droite'):
        super().__init__(titre, auteur, prix, nbrpages, genre)
        self.is_color = is_color
        self.lecture = lecture

    def color(self):
        if self.is_color is True:
            print('couleur')
        else:
            print('noir et blanc')
        return self.is_color


b1 = BD("Lucky Luke", "Morris", 10.40, 45, True)
b2 = BD("Tintin", "Herge", 200.40, 45, False)
print(b1.titre, b1.auteur, b1.prix, b1.nbrpages, b1.is_color, b1.genre, b1.lecture)
print(b2.titre, b2.auteur, b2.prix, b2.nbrpages, b2.is_color, b2.genre, b2.lecture)
couleur = b1.color()
couleur1 = b2.color()


class Mangas(Livre):
    def __init__(self, titre, auteur, prix, nbrpages, is_color='noir et blanc', genre='action', lecture='de droite a gauche'):
        super().__init__(titre, auteur, prix, nbrpages, genre)
        self.is_color = is_color
        self.lecture = lecture

    def affich(self): 
        print(f'le  titre est {self.titre} lauteurs est : {self.auteur} ca coute {self.prix} le nbrpages est {self.nbrpages} le couleur est{self.is_color} la lectrure est de{self.lecture}')
m1 = Mangas("One piece", "Eiichirō Oda", 5.40, 62)
m2 = Mangas("Death Note", "Tsugumi Ōba", 7.40, 75)
m1.affich()
m2.affich()

class Roman(Livre):
    def __init__(self, titre, auteur, prix, nbrpages, nbr_chap=0, resume=''):
        super().__init__(titre, auteur, prix, nbrpages)
        self.nbr_chap = nbr_chap
        self.resume = resume
    def afficher(self)  :  
        print(f'le  titre est {self.titre} lauteurs est : {self.auteur} ca coute {self.prix} le nbrpages est {self.nbrpages} le nombre de chapitre est {self.nbr_chap} et le resume est {self.resume}')       
r1 = Roman("Dora", "Dora", 300, 3.5)
r1.nbr_chap= 12 
r1.resume='un beau roman '
r1.afficher()

class LivreRecette(Livre):
    def __init__(self, titre, auteur, prix, nbrpages, nbr_recette, genre='Recette'):
        super().__init__(titre, auteur, prix, nbrpages, genre)
        self.nbr_recette = nbr_recette

    def ajoutrecette(self, recette):
        self.recette = recette
        self.nbr_recette = self.nbr_recette + 1
    def aff(self): 
        print(f'le  titre est {self.titre} lauteurs est : {self.auteur} ca coute {self.prix} le nbrpages est {self.nbrpages} le nombre de recette est {self.nbr_recette}')       
         
lrc1 = LivreRecette("Marmiton", "Philippe Etchebest", 15.98, 110,10)
lrc1.aff()
class Recette:
    def __init__(self, nom, description, niveau_diff, liste_etapes=['Etape 1']):
        self.nom = nom
        self.description = description
        self.niveau_diff = niveau_diff
        self.liste_etapes = liste_etapes

    def ajoutetape(self, etape):
        self.liste_etapes.append(etape)
rc1 = Recette("Les pâtes crues", "Comment réaliser de délicieuses pâtes crues", 3)
print(rc1.nom, rc1.description, rc1.niveau_diff, rc1.liste_etapes)
rc1.ajoutetape("Ne pas les faire cuire.")
print(rc1.nom, rc1.description, rc1.niveau_diff, rc1.liste_etapes)
rc1.ajoutetape("Sortir les pâtes de leur emballage")
print(rc1.nom, rc1.description, rc1.niveau_diff, rc1.liste_etapes)
lrc1.ajoutrecette(rc1)
print(lrc1.nbr_recette)