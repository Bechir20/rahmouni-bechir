import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
print(df.to_string())
##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
new_df = df.dropna()
print(new_df.to_string())
##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")

df.dropna(inplace = True)

print(df.to_string())
##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
df.fillna(130, inplace = True)
print(df.to_string())
##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
df["Calories"].fillna(130, inplace = True)
print(df.to_string())
##
import pandas as pd

df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")

x = df["Calories"].mean()

df["Calories"].fillna(x, inplace = True)

print(df.to_string())


##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
x = df["Calories"].median()
df["Calories"].fillna(x, inplace = True)
print(df.to_string())
##
 import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
x = df["Calories"].mode()[0]

df["Calories"].fillna(x, inplace = True)

print(df.to_string())
##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
df['Date'] = pd.to_datetime(df['Date'])
df.dropna(subset=['Date'], inplace = True)
print(df.to_string())

##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
df.loc[7,'Duration'] = 45
print(df.to_string())
##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
for x in df.index:
  if df.loc[x, "Duration"] < 120:
    df.loc[x, "Duration"] = 120
print(df.to_string())
##
import pandas as pd
df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
for x in df.index:
    if df.loc[x, "Duration"] > 120:
        df.drop(x, inplace=True)
print(df.to_string())
##
import pandas as pd

df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")

print(df.duplicated())
##
import pandas as pd

df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")

df.drop_duplicates(inplace = True)

print(df.to_string())
## import pandas as pd
import pandas as pd

df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")

print(df.corr())
##
import sys
import matplotlib
matplotlib.use('Agg')

import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv(r"https://www.w3schools.com/python/pandas/dirtydata.csv.txt")

df.plot()

plt.show()

#Two  lines to make our compiler able to draw:
plt.savefig(sys.stdout.buffer)
sys.stdout.flush()


