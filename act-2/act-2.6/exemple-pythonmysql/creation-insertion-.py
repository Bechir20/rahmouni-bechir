# création d'une base de donné 
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="Nadaomar1995."
)
print(mydb) 

mycursor = mydb.cursor()

mycursor.execute("CREATE DATABASE my_database")
##affichage des bases de données: 
import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
  user="root",
  password="Nadaomar1995."
)
mycursor = mydb.cursor()

mycursor.execute("SHOW DATABASES")

for x in mycursor:
    print(x) 

# création d'un tableau
    import mysql.connector
    
    mydb = mysql.connector.connect(
      host="localhost",
      user="root",
      password="Nadaomar1995.",
      database="my_database"
    )
    
    mycursor = mydb.cursor()
    
    mycursor.execute("CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))")    
#création d'un tab avec primary key 
    import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
  user="yourusername",
  password="yourpassword",
  database="mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("CREATE TABLE customers (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), address VARCHAR(255))") 
    
#affichage des tableaux 
import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
  user="root",
  password="Nadaomar1995.",
  database="my_database"
)

mycursor = mydb.cursor()
mycursor.execute("SHOW TABLES")
for x in mycursor:
    print(x) 
    
#ajouter un primary key à un tab existant 
import mysql.connector
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="Nadaomar1995.",
  database="my_database"
)

mycursor = mydb.cursor()

mycursor.execute("ALTER TABLE customers ADD COLUMN id INT AUTO_INCREMENT PRIMARY KEY") 

#inserrer  un seul ligne de données dans le tableau 
import mysql.connector
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="Nadaomar1995.",
  database="my_database"
)
mycursor = mydb.cursor()

sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = ("John", "Highway 21")
mycursor.execute(sql, val)

mydb.commit()

print(mycursor.rowcount, "record inserted.")
#insertion de plusieur données 

import mysql.connector
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="Nadaomar1995.",
  database="my_database"
)
mycursor = mydb.cursor()
sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = [
  ('Peter', 'Lowstreet 4'),
  ('Amy', 'Apple st 652'),
  ('Hannah', 'Mountain 21'),
  ('Michael', 'Valley 345'),
  ('Sandy', 'Ocean blvd 2'),
  ('Betty', 'Green Grass 1'),
  ('Richard', 'Sky st 331'),
  ('Susan', 'One way 98'),
  ('Vicky', 'Yellow Garden 2'),
  ('Ben', 'Park Lane 38'),
  ('William', 'Central st 954'),
  ('Chuck', 'Main Road 989'),
  ('Viola', 'Sideway 1633')
]

mycursor.executemany(sql, val)

mydb.commit()

print(mycursor.rowcount, "was inserted.") 
# selection des données dans le tableau 
import mysql.connector
mydb = mysql.connector.connect(
    host="localhost",
  user="root",
  password="Nadaomar1995.",
  database="my_database"
)
mycursor = mydb.cursor()

mycursor.execute("SELECT name, address FROM customers")

myresult = mycursor.fetchall()

for x in myresult:
  print(x) 
    
