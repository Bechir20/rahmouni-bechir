mydict={'prénon': 'Alex', 'nom': 'Boreau','ville': 'Paris', 'Classe': 'c1'}
print(mydict)

mydict['Email' ] = 'Alex.boreau@gmail.com'
print(mydict)

mydict['Telephone'] = 21650236569
print(mydict)

print(mydict.get('ville'))

del mydict['ville']
print(mydict)

new_dict={'k1':'V1', 'k2':'V2'}
print(new_dict)

mydict.update(new_dict)
print(mydict)