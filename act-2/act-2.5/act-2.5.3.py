def tri_bulle(T):
  
   for i in range(len(T)-1, 0, -1):
       for j in range(0, i):
           if T[j] > T[j+1] :                                                                  
               temp = T[j+1]

               T[j+1] = T[j]

               T[j] = temp

   return T
             
T1 =['Sirius', 'Rigel', 'Bételgeuse', 'Arcturus', 'Aldébaran', 'Véga', 'Deneb', 'Altaïr']
print(tri_bulle(T1))

Etoile=input("Entrez un nom d'étoile : ")
print(Etoile)
T1.append(Etoile)
print(tri_bulle(T1))

def recherche_dichotomique(i, j):
   res = False
   a = 0
   b = len(j)
   m = (a+b)//2
   while (a <= b) and (res==False):
      if j[m] == i:
         res=True
      elif j[m] > i:
         b = m-1
      else :
         a = m+1
      m = (a+b)//2    
   return res   
i=input("Entrez un nom d'étoile pour la recherche:")
if recherche_dichotomique(i,T1) :
   print("L'étoile cherchée existe dans le tableau")
else: 
   print("L'étoile cherchée n'existe pas dans le tableau")
   