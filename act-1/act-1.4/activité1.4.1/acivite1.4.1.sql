use   medic ;
CREATE TABLE medicament (
nom char(255) PRIMARY KEY,
description_C char(255),
description_L char(255),
Pillule  integer);
CREATE TABLE contre_indication (
codeci char(255)  PRIMARY KEY,
descriptionci  char(255),
nom_medicament char(255),
FOREIGN KEY (nom_medicament) REFERENCES medicament (nom));
INSERT INTO medicament (nom, description_C , description_L ,Pillule)
VALUES ('Chourix' ,'Médicament contre la chute des choux', 'Vivamus...', 13);
INSERT INTO medicament (nom, description_C , description_L ,Pillule)
VALUES ('Tropas' , 'Médicament contre les dysfonctionnements intellectuels','Suspendisse...', 42) ;
INSERT INTO contre_indication (codeci, descriptionci , nom_medicament )
VALUES ('CI1' ,'Ne jamais prendre après minuit' ,'Chourix');
INSERT INTO contre_indication (codeci, descriptionci , nom_medicament )
VALUES ('CI2' ,'Ne jamais mettre en contact avec de l''eau', 'Chourix');
INSERT INTO contre_indication (codeci, descriptionci , nom_medicament )
VALUES ('CI3' ,'Garder à l''abri de la lumière du soleil' ,'Tropas');
select* from medicament 
