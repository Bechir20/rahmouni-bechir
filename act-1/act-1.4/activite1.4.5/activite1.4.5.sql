use du-producteur-au-consommateur ;
CREATE TABLE producteur (
raison_sociale  VARCHAR(50)  PRIMARY KEY , 
ville VARCHAR (50)
);
CREATE TABLE consommateur (
login VARCHAR(50),
email VARCHAR( 50),
nom VARCHAR  (50) NOT NULL ,
prenom VARCHAR  (50)NOT NULL  ,
ville  VARCHAR  (50)NOT NULL, 
PRIMARY KEY (login,email),
UNIQUE   (nom,prenom,ville)
);
CREATE TABLE produit (
id INTEGER PRIMARY KEY,
descriptionn VARCHAR (50),
produit_par VARCHAR (50) ,
consomme_par_login VARCHAR (50),
consomme_par_email VARCHAR (50),
FOREIGN KEY (produit_par) REFERENCES Producteur(raison_sociale),
FOREIGN KEY (consomme_par_login,consomme_par_email) REFERENCES Consommateur(login,email)
);
INSERT INTO producteur (raison_sociale, ville)
VALUES ('Pommes Picardes SARL', 'Compiègne');
INSERT INTO produit (id, descriptionn, produit_par)
VALUES (1, 'lot de pommes', 'Pommes Picardes SARL');
INSERT INTO produit (id, descriptionn, produit_par)
VALUES (2, 'lot de pommes', 'Pommes Picardes SARL');
INSERT INTO produit (id, descriptionn, produit_par)
VALUES (3, 'lot de pommes', 'Pommes Picardes SARL');
INSERT INTO produit (id, descriptionn, produit_par)
VALUES (4, 'lot de pommes', 'Pommes Picardes SARL');
INSERT INTO produit (id, descriptionn, produit_par)
VALUES (5, 'lot de cidre', 'Pommes Picardes SARL');
INSERT INTO produit (id, descriptionn, produit_par)
VALUES (6, 'lot de cidre', 'Pommes Picardes SARL');
INSERT INTO consommateur (login, email, nom, prenom, ville) 
VALUES ('Al', 'Al.Un@compiegne.fr', 'Un', 'Al', 'Compiègne');
INSERT INTO consommateur (login, email, nom, prenom, ville)
VALUES ('Bob', 'Bob.Deux@compiegne.fr', 'Deux', 'Bob', 'Compiègne');
INSERT INTO consommateur (login, email, nom, prenom, ville)
VALUES ('Charlie', 'Charlie.Trois@compiegne.fr', 'Trois', 'Charlie', 'Compiègne');
UPDATE produit
SET consomme_par_login='Al', consomme_par_email ='Al.Un@compiegne.fr'
where id=1;
UPDATE produit
SET consomme_par_login='Bob', consomme_par_email='Bob.Deux@compiegne.fr'
WHERE id=2 OR id= 4 ;
UPDATE produit
SET consomme_par_login='Al', consomme_par_email='Al.Un@compiegne.fr'
WHERE descriptionn = 'lot de cidre' ; 
DELETE FROM consommateur
WHERE login='Charlie' ;
