Question1 
1/ une mesure :  est un nombre ou une valeur. Elle peut être additionnée à une autre.
    un KPI : sont composées d’une ou plusieurs mesures. Elles sont souvent liées à un objectif.

2/ Une mesure non additive ne peut être agrégée.La seule chose possible est d’effectuer des comptages ou des moyennes avec la fonction(DistinctCount).

3/  Les fonctions d’agrégations peuvent s'appliquer sur des faits additifs : 

Sum :Calcule la somme des valeurs .
Count : Renvoie le nombre total. 

 Les fonctions d’agrégations peuvent s'appliquer sur des faits semi_additifs : 
Min : Renvoie la plus petite valeur.
Max :Renvoie la plus grande valeur .
ByAccount : Calcule l'agrégation conformément à la fonction d'agrégation affectée au type de compte d'un membre d'une dimension de comptes. 
AverageOfmesures : Calcule la moyenne des valeurs de tous les mersures vides.
Firstmesure: Renvoie la valeur du premier mesure.
Lastmesure : Renvoie la valeur du dernier mesure .
FirstNonEmpty : Renvoie la valeur du premier mesure non vide.
LastNonEmpty : Renvoie la valeur du dernier mesure non vide.

4/pour une dimension les clés spéciales servent à  définir :
 Date effective : c’est la date à la quelle l’enregistrement à été crée, de préférence dans le système d’enregistrements (System of records).
 Date retrait : C’est la date à laquelle l’enregistrement a été retiré du système d’enregistrements.
 Indicateur effectif : En général est ‘O’ si l’enregistrement est toujours actif (Date retrait non nulle), ‘N’ sinon.
5/ 
Une clé de substitution  est une clé non intelligente utilisée afin de substituer la clé naturelle (Business Key) qui provient des systèmes opérationnels. 
les clés naturelles est une clé (en général clé primaire) choisie parmi les clés candidates . 
6/ l’utilité d’une clé de substition/artificielle (surrogate key) :

    Remplacer la clé naturelle : Effectivement une clé de substitution remplace la clé artificielle en terme d’utilisation, ce n’est plus la clé naturelle qui sera utilisé pour faire les jointures avec les tables de faits ou les autres tables de dimension (niveaux hiérarchiques dans le cas d’une dimension en flocons de neiges);
    Complèter l’information :La clé de substitution n’a aucun sens en terme d’affaire, elle est utilisée dans l’entrepôt de données seulement ! et on aura toujours besoin de la clé artificielle ou naturelle dans la dimension pour pouvoir faire la correspondance entre l’élément de dimension (un client par exemple) dans l’entrepôt de données et l’élément de la table des clients dans le système opérationnel.
7/ 
-Modèle en étoile: C’est une manière de relier une dimension à un fait dans un entrepôt de données. Dans le modèle en étoile on a une table de fait centrale qui est liée par les tables de dimensions dénormalisées.
Les dimensions ne sont pas liées entre elles.
-Modèle en flocon de neige: C’est une manière de relier une dimension à un fait dans un entrepôt de données. C’est le modèle en étoile avec une normalisation des dimensions. Il peut exister des hiérarchies des dimensions 
pour diviser les tables de dimensions lorsqu’elles sont trop volumineuses.
-Modèle en constellation: Se compose donc de plusieurs tables des faits avec leurs tables de dimensions respectives.

8/
-Identifier le périmètre fonctionnel candidat. 
-Déterminer l'objectif et les événements de gestion à suivre.
-Estimer la volumétrie du périmètre.
-Analyse fonctionnelle, recueil des besoins utilisateurs.
-Conception de l'architecture technique détaillée.
-Etablir une démarche générique de mise en œuvre.
-Les apports d'une démarche itérative, le contenu d'une itération.
-Première itération ou projet pilote, bien le choisir. Rôle du sponsor, de la MOA, de la MOE, impact sur l'organisation.
-L'administration et le suivi de la solution opérationnelle.

9/Un fait statique, comme son nom semble l’indiquer, est composé des mesures statiques, visibles telles qu’elles ont été conçues. Attention, cela ne signifie pas qu’elles sont sans mouvement ou sans animation. 
  un fait dynamique :  tout le contraire d'un fait statique. Les mesures seront présentées de façon différente selon l’interaction avec l'utilisateur  . 



10/ les 3 types de dimensions à évolution lente (slowly changing dimensions):

    Type 1 :Ecriture en surimpression:  Écraser l'ancienne valeur avec la nouvelle
    Type 2 :Créer un autre enregistrement de la dimension: Ajouter une ligne dans la table de dimension pour la nouvelle valeur

    Type 3 :  Création d'un champ de valeur courante :Avoir deux colonnes dans la table dedimension correspondant à l'ancienne et la nouvellevaleur

Le schéma du SCD Type 2 devrait inclure des colonnes spécifiques au SCD contenant les informations de log standard, notamment:
    start : ajoute une colonne au schéma de la table SCD contenant la date de début d'un enregistrement.
    end : ajoute une colonne au schéma de la table SCD contenant la date de fin d'un enregistrement.
    version : ajoute une colonne au schéma de la table SCD contenant le numéro de version de l'enregistrement. 

10/ on peut avoir des tables de faits sans fait pour établir des relations entre des éléments de dimensions différentes. 